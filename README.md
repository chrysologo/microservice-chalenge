# Microservice Challenge!

## Schema

![solution schema](./schema.svg)

It is an implementation to explore how to develop microservices using Python Flask, Docker and Mongo DB. 

Bearing in mind the schema above, our business team could, at least, run tests in your local environment.