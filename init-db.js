// Create inital table
db = db.getSiblingDB("unique_ips")

// Create table and drop if is available by default
db.unique_ips_tb.drop()

// 'foobar' data
db.unique_ips_tb.insertMany([
    {
        "counter": 0,
        "ip": "192.168.1.2"
    },
    {
        "counter": 23,
        "ip": "34.168.34.2"
    },
]);